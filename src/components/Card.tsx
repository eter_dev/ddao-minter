import React from "react";
import "./Card.css";

type WithChildren<T = {}> = T & { children?: React.ReactNode };

type CardProps = WithChildren<{

}>;

function Card({ children }: CardProps) {
  return <div className="card">{children}</div>;
}

export default Card;
