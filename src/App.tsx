import React, { ChangeEventHandler, MouseEventHandler, useState } from "react";
import { ethers } from "ethers";
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";
import Card from "./components/Card";
import Background from "./components/Background";
import Logo from "./assets/ddao.jpg";
import twitterLogo from "./assets/twitter.svg";

import { ToastContainer, toast, Slide } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./App.css";

import MINT_CONTRACT from "./artifacts/contracts/ddao.sol/Dev.json";
const CONTRACT_ADDRESS = "0x25ed58c027921e14d86380ea2646e3a1b5c55a8b";
const ERROR_CODE_TX_REJECTED_BY_USER = 4001;
const MAINNET_NETWORK_ID: number = 1;
let _provider: any = null;
let _signer: any = null;
let Dev: Partial<ethers.Contract>;

// const metamaskInstalled = (): boolean => {
//   if (!(window as any).ethereum) {
//     return false;
//   }
//   return true;
// };

const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider, // required
    options: {
      infuraId: "86ddbc7b94ff40af908eaed373ac95d6", // required
    },
  },
};

const web3Modal = new Web3Modal({
  network: "mainnet", // optional
  providerOptions, // required
  theme: "dark",
});

function App() {
  const [userWallet, setUserWallet] = useState("");
  const [tokenID, setTokenID] = useState("");
  const [networkError, setNetworkError] = useState(false);

  // if (!metamaskInstalled()) {
  //   //Show Metamask not installed Component
  // }

  const fetchAccountData = async () => {
    const _web3 = new ethers.providers.Web3Provider(_provider);
    _signer = _web3.getSigner();
    Dev = new ethers.Contract(CONTRACT_ADDRESS, MINT_CONTRACT.abi, _signer);

    const accounts = await _web3.listAccounts();
    const { chainId } = await _web3.getNetwork();

    console.log(chainId);

    // MetaMask does not give you all accounts, only the selected account
    console.log("Got accounts", accounts);
    const selectedAccount = accounts[0];

    setUserWallet(selectedAccount);
    _checkNetwork(chainId);
  };

  const _checkNetwork = (chainId: number) => {
    if (chainId === MAINNET_NETWORK_ID) {
      return true;
    }
    setNetworkError(true);
  };

  const connectWallet = async () => {
    web3Modal.clearCachedProvider();

    console.log("Opening a dialog", web3Modal);
    try {
      _provider = await web3Modal.connect();
    } catch (e) {
      console.log("Could not get a wallet connection", e);
      return;
    }

    // Subscribe to accounts change
    _provider.on("accountsChanged", (accounts: string) => {
      fetchAccountData();
    });

    // Subscribe to chainId change
    _provider.on("chainChanged", (chainId: string) => {
      fetchAccountData();
    });

    // Subscribe to networkId change
    _provider.on("networkChanged", (networkId: string) => {
      setNetworkError(false);
      fetchAccountData();
    });

    await fetchAccountData();
  };

  const tokenNameHandler: ChangeEventHandler<HTMLInputElement> = (event) => {
    setTokenID(event.target.value);
  };

  const createTokenHandler: MouseEventHandler<HTMLButtonElement> = async (
    event
  ) => {
    try {
      // If a transaction fails, we save that error in the component's state.
      // We only save one such error, so before sending a second transaction, we
      // clear it.

      // We send the transaction, and save its hash in the Dapp's state. This
      // way we can indicate that we are waiting for it to be mined.
      const tx = await Dev.claim(tokenID);

      //Alert transaction is in progress
      toast.warn("Transaction Being Sent", {
        toastId: "tx-sending",
      });

      // We use .wait() to wait for the transaction to be mined. This method
      // returns the transaction's receipt.
      const receipt = await tx.wait();

      // The receipt, contains a status flag, which is 0 to indicate an error.
      if (receipt.status === 0) {
        // We can't know the exact error that make the transaction fail once it
        // was mined, so we throw this generic one.
        throw new Error("Transaction failed");
      }

      // If we got here, the transaction was successful, so you may want to
      // update your state. Here, we update the user's balance.
    } catch (error: any) {
      // We check the error code to see if this error was produced because the
      // user rejected a tx. If that's the case, we do nothing.
      if (error.code === ERROR_CODE_TX_REJECTED_BY_USER) {
        // resetFields();
        toast.error("Transaction Cancelled by User", {
          toastId: "tx-failed",
        });
      }
      // Other errors are logged and stored in the Dapp's state. This is used to
      // show them to the user, and for debugging.
      console.error(error);
      toast.error("Transaction Failed", {
        toastId: "tx-failed",
      });
      setTokenID("");
      return;
    }
    // If we leave the try/catch, we aren't sending a tx anymore, so we clear
    // this part of the state.
    toast.success("Token Minted!", {
      toastId: "tx-minted",
    });
    setTokenID("");
  };

  return (
    <div className="wrapper">
      <ToastContainer
        position="top-right"
        autoClose={5000}
        transition={Slide}
      />
      <Background></Background>
      <div className="App">
        <img className="logo" src={Logo} alt="Developer Dao Logo"></img>
        <Card>
          <p className="text header"> Instructions</p>
          <p className="text body">
            1. Find an Available Token ID to mint {String.fromCharCode(32)}
            <a href="http://ddao.ibby.dev" target="_blank" rel="noreferrer">
              here
            </a>
          </p>
          <p className="text body">
            2. Connect to Wallet, Enter Token ID and click Mint!
          </p>
        </Card>
        <Card>
          <header>DDAO Token Minter</header>
          {!userWallet && (
            <button className="buttons" onClick={connectWallet}>
              Connect Wallet
            </button>
          )}
          {userWallet && (
            <input
              value={tokenID}
              className="inputs"
              onChange={tokenNameHandler}
              type="text"
              placeholder="Enter Token ID"
            ></input>
          )}
          <button
            className="buttons"
            onClick={createTokenHandler}
            disabled={!userWallet || networkError}
          >
            Mint your token
          </button>
          {networkError && (
            <p className="network_error">
              Please Connect to the Ethereum Network
            </p>
          )}
        </Card>
        <div className='twitter'>
          <a href='https://twitter.com/IbbyDev' target="_blank" rel="noreferrer">
          <img src={twitterLogo} alt="Twitter Logo"></img>
          </a>
          <p>@IbbyDev</p>
        </div>
      </div>
    </div>
  );
}

export default App;
